import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';
// import { TextInput } from 'react-native'; changed with Input.js
import { Button, Card, CardSection, Input, Spinner } from './common';


class LoginForm extends Component {
    state = { email: '', password: '', error: '', success: '', loading: false };

    //helper or method for button
    onButtonPress() {
        const { email, password } = this.state;
        //to clean up code. and give access  to email and password var

        this.setState({ error: '', success: '', loading: true });
        
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(() => { //when promise rejected, user'll be thrown to create ID
                firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(this.onLoginSuccess.bind(this))    
                .catch(this.onLoginFail.bind(this));
            });
    }

    onLoginFail() {
        this.setState({ error: 'Authentication Failed', loading: false });
    }

    //make method to stop loading activity
    onLoginSuccess() {
        this.setState({
            email: '',
            password: '',
            loading: false,
            error: '',
            success: 'Login Success',
        });
    }

    //make helper method to change button icon with spinner when loading
    renderButton() {
        if (this.state.loading) {
            return <Spinner size="small" />;
        }
        return (
            <Button whenPressed={this.onButtonPress.bind(this)}>
                Login
            </Button>
        );
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input //from TextInput to Input
                        placeholder="user@gmail.com"
                        label="Email"
                        value={this.state.email} 
                        onChangeText={email => this.setState({ email })}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        secureTextEntry
                        placeholder="password"
                        label="Password"
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                    />
                </CardSection>
                
                <Text style={styles.errorStyle}>
                    {this.state.error}
                </Text>

                <Text style={styles.successStyle}>
                    {this.state.success}
                </Text>

                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>            
        );
    }
}

const styles = {
    errorStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    successStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'green'
    }
};

export default LoginForm;
